# Introduction to Design Patterns

We want to design software that is easy to maintain and easy to modify. But, the obvious solutions to design problems can lead to brittle software that cannot be modified or maintained easily. Design patterns represent common software design problems and well-tested solutions to those problems.

## Content Learning Objectives

After completing this activity, students should be able to:

- Identify particular code design problems and apply the Strategy design pattern to solve them.

## Process Skill Goals

During the activity, students should make progress toward:

- Reading diagrams and reasoning about the code structure that they represent. (Critical Thinking)

## Team Roles

Record role assignments here.

Role | Team Member
:-- | :--
Manager |
Presenter |
Recorder |
Reflector |

## Model 1: The Duck Simulator v1

You are working at a company that builds duck simulator software. This is the design of the current version of the software.

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class Duck {
    +quack()
    +swim()
    +{abstract}display()
}

class MallardDuck extends Duck {
    +display()
}

class RedHeadDuck extends Duck {
    +display()
}

class "Other Ducks" extends Duck {
    Lots of other
    Duck types...
}
@enduml
```

### Questions (5 min)

1. What is the same about all types of Ducks?
2. What is different for each type of Duck?
3. Your boss wants you to have Ducks be able to fly. What change would you make so that would be possible?

 The Duck Simulator example is from [Head First Design Patterns, 2nd Edition](https://learning.oreilly.com/library/view/head-first-design/9781492077992/), Eric Freeman and Elizabeth Robson, O’Reilly Media, Inc., 2020

***STOP - Do not continue until asked to do so by the instructor.***

## Model 2: The Duck Simulator v1.1

Let's try using inheritance to add a fly() behavior in the superclass...

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class Duck {
    +quack()
    +swim()
    +{abstract}display()
    +fly()
}

class MallardDuck extends Duck {
    +display()
}

class RedHeadDuck extends Duck {
    +display()
}

class "Other Ducks" extends Duck {
    Lots of other
    Duck types...
}
@enduml
```

Once you have implemented this change, your boss comes back and wants to add a new Duck type. He wants you to add a RubberDuck type. 

RubberDucks don’t fly. RubberDucks don’t quack either, they squeak.

### Questions (3 min)

1. When you extend the Duck type to create RubberDuck, what are you going to have to do to implement the RubberDuck’s special quack behavior and lack of flying behavior?

***STOP - Do not continue until asked to do so by the instructor.***

## Model 3: The Duck Simulator v1.2

Let's try overriding... 

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class Duck {
    +quack()
    +swim()
    +{abstract}display()
    +fly()
}

class MallardDuck extends Duck {
    +display()
}

class RedHeadDuck extends Duck {
    +display()
}

class "Other Ducks" extends Duck {
    Lots of other
    Duck types...
}

class RubberDuck extends Duck {
    +quack()
    +display()
    +fly()
}
note left of RubberDuck::quack
    overridden to squeak()
end note
note left of RubberDuck::fly
    overridden to do nothing
end note
@enduml
```

### Questions (6 min)

1. Hopefully this is the last “odd” type of duck. What will happen if we are asked to add another Duck type that is significantly different from the “usual” types of Ducks?
2. Which of the design smells (Rigidity, Fragility, Immobility, Viscosity, Needless Complexity, Needless Repetition, Opacity) from Activity 3 do you see here? (You can go back and look at Activity 3 if you need a reminder.) Explain why you chose that design smell.

***STOP - Do not continue until asked to do so by the instructor.***

## Model 4: The Duck Simulator v1.3

Your boss has asked for another type of “odd” duck - a DecoyDuck, which doesn’t fly or quack!

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class Duck {
    +quack()
    +swim()
    +{abstract}display()
    +fly()
}

class MallardDuck extends Duck {
    +display()
}

class RedHeadDuck extends Duck {
    +display()
}

class "Other Ducks" extends Duck {
    Lots of other
    Duck types...
}

class RubberDuck extends Duck {
    +quack()
    +display()
    +fly()
}
note left of RubberDuck::quack
    overridden to squeak()
end note
note left of RubberDuck::fly
    overridden to do nothing
end note

class DecoyDuck extends Duck {
    +quack()
    +display()
    +fly()
}
note right of DecoyDuck::quack
    overridden to do nothing
end note
note right of DecoyDuck::fly
    overridden to do nothing
end note
@enduml
```

### Questions (5 min)

1. This is starting to look like it’s going to be an ongoing problem. Describe the problem that inheritance is causing.
2. Do you still stand by your choice of design smell?
3. Do you have any ideas for how we could redesign this?

***STOP - Do not continue until asked to do so by the instructor.***

## Model 5: The Duck Simulator v2?

Let's try using Interfaces...

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

abstract class Duck {
    +swim()
    +{abstract}display()
}

class MallardDuck extends Duck implements Flying, Quacking {
    +display()
    +fly()
    +quack()
}

class RedHeadDuck extends Duck implements Flying, Quacking {
    +display()
    +fly()
    +quack()
}

class "Other Ducks" extends Duck {
    Lots of other
    Duck types...
}

class RubberDuck extends Duck implements Quacking{
    +display()
    +quack()
}
note right of RubberDuck::quack
    overridden to squeak()
end note

class DecoyDuck extends Duck {
    +display()
}

interface Flying <<Interface>> {
    +{abstract}fly()
}

interface Quacking <<Interface>> {
    +{abstract}quack()
}
@enduml
```

### Questions (12 min)

1. How has this solved the problem we were having with inheritance?
2. What new problem is this going to introduce as the number of Duck subtypes increases?
3. What Duck behaviors are the same for all Duck types?
4. What Duck behaviors ***might*** be different for different Duck types, *but are **not** different for **every** Duck type*?
5. What problem is likely to happen with the following code?

```java
Collection<Duck> pond;
// There is code here to create a pond and fill with various types of Ducks
for (Duck duck: pond) {
    duck.quack();
}
```

6. Which of the design smells (Rigidity, Fragility, Immobility, Viscosity, Needless Complexity, Needless Repetition, Opacity) from Activity 3 do you see here? Explain why you chose that design smell?

The relevant design principle here is:
>***Encapsulate what varies.*** *Identify the aspects of your application that vary, and separate them from what stays the same.*

***STOP - Do not continue until asked to do so by the instructor.***

## Model 6: The Duck Simulator v2

What we need are sets of behaviors that provide the same operations, but can have different implementations.

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

interface FlyBehavior <<Interface>> {
    +{abstract}fly()
}

interface QuackBehavior <<Interface>> {
    +{abstract}quack()
}

class FlyWithWings implements FlyBehavior {
    +fly()
}
note bottom of FlyWithWings
    Implements 
    duck flying
    with wings
end note

class FlyNoWay implements FlyBehavior {
    +fly()
}
note bottom of FlyNoWay
    Does
nothing
end note

class Quack implements QuackBehavior {
    +quack()
}
note bottom of Quack
    Quacking
    sound
end note

class Squeak implements QuackBehavior {
    +quack()
}
note bottom of Squeak
    Squeaking
    sound
end note

class MuteQuack implements QuackBehavior {
    +quack()
}
note bottom of MuteQuack
    No sound
end note
@enduml
```

### Questions (5 min)

1. What do we need to do if we want to add a new fly() or quack() behavior?
2. Do we need to modify any other fly() or quack() behavior?

The relevant design principle here is:
>***Program to an interface, not an implementation.***

***STOP - Do not continue until asked to do so by the instructor.***

## Model 7: The Duck Simulator v2 - The Big Picture

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

together {
    class Duck {
        -flyBehavior : FlyBehavior
        -quackBehavior : QuackBehavior
        +swim()
        +{abstract}display()
        +performFly()
        +performQuack()
        +setFlyBehavior(FlyBehavior)
        +setQuackBehavior(QuackBehavior)
    }

    class MallardDuck extends Duck {
        +display()
    }

    class RedHeadDuck extends Duck {
        +display()
    }

    class "Other Ducks" extends Duck {
        Lots of other
        Duck types...
    }

    class RubberDuck extends Duck {
        +display()
    }

    class DecoyDuck extends Duck {
        +display()
    }
}

together {
    interface FlyBehavior <<Interface>> {
        +{abstract}fly()
    }

    class FlyWithWings implements FlyBehavior {
        +fly()
    }
    note bottom of FlyWithWings
        Implements 
        duck flying
        with wings.
    end note

    class FlyNoWay implements FlyBehavior {
        +fly()
    }
    note bottom of FlyNoWay
        Does
        nothing
    end note
}

together {
    interface QuackBehavior <<Interface>> {
        +{abstract}quack()
    }
    class Quack implements QuackBehavior {
        +quack()
    }
    note bottom of Quack
        Quacking
        sound.
    end note

    class Squeak implements QuackBehavior {
        +quack()
    }
    note bottom of Squeak
        Squeaking
        sound.
    end note

    class MuteQuack implements QuackBehavior {
        +quack()
    }
    note bottom of MuteQuack
        No
        sound.
    end note
}

Duck *--- FlyBehavior
Duck *--- QuackBehavior
@enduml
```

### Questions (15 min)

1. Which associations in the diagram are IS-A relationships?
2. Which associations in the diagram are HAS-A relationships?
3. How do we assign FlyBehaviors or QuackBehaviors to individual Ducks?
    - When does this happen? Compile time or run time?
4. Imagine we had a single instance of a MallardDuck whose wing had been injured.
    - Would it be possible to change its FlyBehavior so that it could not fly?
    - How would you do that?
5. If we developed a new FlyBehavior or QuackBehavior, would it ever involve modifying the code for Duck?

Each Duck has a FlyBehavior and a QuackBehavior. Instead of inheriting behavior, they get their behavior from composition.

The relevant design principle here is:
>***Favor composition over inheritance.***

***STOP - Do not continue until asked to do so by the instructor.***

## Model 8: The Strategy Design Pattern

```plantuml
@startuml
skinparam classAttributeIconSize 0
hide circle
hide empty attributes
hide empty methods

class Context {
    +ContextInterface()
}

interface Strategy {
    +{abstract}AlgorithmInterface()
}

class ConcreteStrategyA implements Strategy {
    +AlgorithmInterface()
}

class ConcreteStrategyB implements Strategy {
    +AlgorithmInterface()
}

class ConcreteStrategyC implements Strategy {
    +AlgorithmInterface()
}

Context "strategy" *-right- Strategy
@enduml
```

- Defines a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from the clients that use it.
- Object Behavioral Pattern
- Use when
  - Many related classes differ only in their behavior.
  - You need different variants of an algorithm.
  - An algorithm uses data  that the clients shouldn’t know about. Hide complex, algorithm-specific data structures.
  - A class defines many behaviors, and these appear as multiple conditional statements in its operations. Instead of many conditionals, move related conditional branches into their own Strategy class.
- Benefits:
  - Families of related algorithms.
  - An alternative to subclassing. Lets you vary the algorithm independently of context.
  - Strategies eliminate conditional statements.
  - A choice of implementations.
- Drawbacks:
  - Clients must be aware of different strategies. (to be able to select the correct one.)
  - Communication overhead between Strategy and Context. (complex strategies may require more information than is needed by simple strategies.)
  - Increased number of objects.

[Strategy Pattern](https://learning.oreilly.com/library/view/design-patterns-elements/0201633612/ch05.html#ch05lev1sec9) adapted from [Design Patterns: Elements of Reusable Object-Oriented Software](https://learning.oreilly.com/library/view/design-patterns-elements/0201633612/), by Erich Gamma, Richard Helm, Ralph Johnson, John Vlissides, [Addison-Wesley Professional](https://learning.oreilly.com/library/publisher/addison-wesley-professional/), 1995

### Questions (8 min)

1. Which part of Model 7 matches each of the parts of the diagram in Model 8?
    - The Context?
    - A ContextInterface?
    - A Strategy?
    - A Concrete Strategy?
2. Which of the **Use when** items match the DuckSimulator problem?
3. Which of the **Benefit** items match the DuckSimulator problem?
4. Which of the **Drawback** items match the DuckSimulator problem?

***STOP - Do not continue until asked to do so by the instructor.***

## Model 9: Design Patterns and Other OO Design Principles

### Design Patterns Provide:

- Experience reuse
  - Wisdom and lessons learned from other developers in similar situations.
- A shared vocabulary
  - Let you communicate complex architectures without having to describe things at the class level.
  - Improved modifiability and maintainability of code.

### Applying Design Patterns:

- Often not something you do at the beginning of a design.
- Often something you do once you try to modify an existing design, and it becomes obvious that something is wrong with the design.
  - Then a pattern is applied to make the design better.

### Other OO Design Principles

- High Cohesion
  - Designed around a set of related functions.
  - Does one thing.
  - Single Responsibility Principle (SRP).
- Low Coupling
  - When two objects are loosely coupled, they can interact, but have very little knowledge of each other.
- Open-Closed Principle
  - Classes should be open for extension, but closed for modification.

### Questions (9 min)

1. Describe how High Cohesion applies to one class in the DuckSimulator.
2. Describe how Low Coupling applies to two classes in the DuckSimulator.
3. Describe how the DuckSimulator v2 follows the Open-Closed Principle.

Copyright © 2024 Karl R. Wurst.

<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
